import { Component, OnInit } from '@angular/core';
import { CounterService } from "../counter.service";

@Component({
  selector: 'app-screen',
  templateUrl: './screen.component.html',
  styleUrls: ['./screen.component.scss']
})
export class ScreenComponent implements OnInit {

  screenCounter: number = 0;

  constructor( private counterService: CounterService ) {
    this.screenCounter = this.counterService.counter;
  }

  ngOnInit() {
    // We listen counterChange events to retrieve counter changes on CounterService
    this.counterService.counterChange.subscribe(
      data => {
        // We update screen counter with new value
        this.screenCounter = data;
        console.log('UPDATE SCREEN | new value: ' + this.screenCounter);
        console.log('---------------------------');
      }
    );
  }

}
